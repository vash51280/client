(function(angular) {
    'use strict';

    angular.module('gitlabKBApp.board').controller('BoardController', 
        [
            '$scope', 
            '$http', 
            '$stateParams',
            'BoardService', 
            'board', 
            '$state',
            '$window',
            function ($scope, $http, $stateParams, BoardService, board, $state, $window) {
                if (_.isEmpty(board.stages)) {
                    $state.go('board.import', {project_id: $stateParams.project_id});
                }

                $window.scrollTo(0, 0);

                $scope.board = board;
                $scope.state = $state;

                $scope.dragControlListeners = {
                    itemMoved: function (event) {
                        var card = event.source.itemScope.card;
                        var newLabel = event.dest.sortableScope.$parent.stage.label;

                        var pattern = /KB\[stage\]\[\d\]\[(.*)\]/;
                        card.labels = _.filter(card.labels, function(label) {
                            return !pattern.test(label);
                        });

                        card.labels.push(newLabel);

                        $http.put('/api/card', {
                            project_id: card.project_id,
                            issue_id: card.id,
                            title: card.title,
                            labels: card.labels.join(', ')
                        }).success(function (data) {
                            
                        });
                    },
                    orderChanged: function (event) {
                    },
                    containment: '#board'
                };
            }
        ]
    );
})(window.angular);

