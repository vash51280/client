(function(angular) {
'use strict';
/**
* @todo избавиться от блоков resolve v будущем
**/
angular.module('gitlabKBApp.board', ['ui.router', 'ui.sortable', 'mm.foundation.dropdownToggle', 'gitlabKBApp.websocket', 'll.markdown','ll.modal']).config(['$stateProvider', function ($stateProvider) {
    $stateProvider
        .state('board', {
            url: '/boards',
            views: {
                "": {
                    templateUrl: 'assets/html/board/views/index.html'
                },
                "top-bar@board": {
                    templateUrl: 'assets/html/board/views/top_bar.html',
                    controller: 'TopBarController'
                }
            },
            data: { 
                access: 1
            }
        })
        .state('board.boards', {
            url: '/',
            views: {
                "content@board": {
                    templateUrl: 'assets/html/board/views/board/boards.html',
                    controller: 'BoardListController'
                }
            },
            data: { 
                access: 1
            }
        })
        .state('board.cards', {
            url: '/:project_id',
            views: {
                "content@board": {
                    templateUrl: 'assets/html/board/views/board/cards.html',
                    controller: 'BoardController',
                    resolve: {
                        board: board
                    }
                },
                "top-bar@board": {
                    templateUrl: 'assets/html/board/views/top_bar.html',
                    controller: 'TopBarController'
                }
            },
            onEnter: ['WebsocketService', '$stateParams', function(WebsocketService, $stateParams) {
                WebsocketService.emit('board.view', {board: $stateParams.project_id});
            }],
            data: { 
                access: 1
            }
        })
        .state('board.import', {
            url: '/:project_id/import',
            views: {
                "content@board": {
                    templateUrl: 'assets/html/board/views/board/configuration.html',
                    controller: 'ConfigurationController'
                }
            },
            data: { 
                access: 1
            }
        })
        .state('board.cards.create', {
            url: '/cards/new',
            views: {
                "modal@board": {
                    templateUrl: "assets/html/board/views/card/create.html",
                    controller: 'NewIssueController',
                    resolve: {
                        users: users,
                        milestones: milestones
                    }
                }
            },
            data: { 
                access: 1
            }
        })
        .state('board.cards.view', {
            url: '/cards/:issue_id',
            views: {
                "modal@board": {
                    templateUrl: "assets/html/board/views/card/view.html",
                    controller: 'ViewController'
                }
            },
            data: { 
                access: 1
            }
        });
}]).config(['$markdownProvider', function($markdownProvider) {
    $markdownProvider.config({linkify: true});
    $markdownProvider.registerPlugin(window.merge_request_plugin);
}]);

board.$inject = ['BoardService', '$stateParams'];
function board(BoardService, $stateParams) {
    return BoardService.get($stateParams.project_id);
}

users.$inject = ['UserService', '$stateParams'];
function users(UserService, $stateParams) {
    return UserService.list($stateParams.project_id);
}

milestones.$inject = ['MilestoneService', '$stateParams'];
function milestones(MilestoneService, $stateParams) {
    return MilestoneService.list($stateParams.project_id);
}

})(window.angular);
