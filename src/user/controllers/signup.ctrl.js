(function() {
    'use strict';

    angular.module('gitlabKBApp.user').controller('SignupController', 
        [
            '$scope', 
            '$http', 
            '$state', 
            'AuthService', 
            function ($scope, $http, $state, AuthService) {
                $scope.user = {};
                $scope.isSaving = false;

                $scope.signup = function () {
                    $scope.isSaving = true;
                    AuthService.register($scope.user).then(function (result) {
                        AuthService.authenticate($scope.user).then(function(auth) {
                            $http.defaults.headers.common['X-KB-Access-Token'] = auth;
                            $state.go('tokenCreate');
                        });
                    });
                };
            }
        ]
    );
})(window.angular);

